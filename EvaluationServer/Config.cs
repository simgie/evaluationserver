﻿using System.Collections.Generic;
using System.IO;

namespace EvaluationServer
{

    class Config : File
    {
        Dictionary<string, string> settings = new Dictionary<string, string>();

        public Config(string location)
        {
            this.location = location;

            sr = new StreamReader(location);
            string line;
            while((line = sr.ReadLine()) != null)
            {
                int index = line.IndexOf(":") + 2;
                //Console.WriteLine(line.Substring(0, index - 2));
                //Console.WriteLine(line.Substring(index));
                settings.Add(line.Substring(0, index - 2), line.Substring(index));

            }
            sr.Close();
        }

        public string get(string val)
        {
            try {
                return settings[val];
            }
            catch (System.Collections.Generic.KeyNotFoundException)
            {
                return "e;outofq;";
            }
        }
    }
}
