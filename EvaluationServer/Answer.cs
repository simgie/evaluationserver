﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluationServer
{
    class Answer : File
    {
        public Answer(string location)
        {
            this.location = location;
            init();
        }

        public void add(string val)
        {
            List<string> list = read();
            list.Add(val);
            write(list);
        }

    }
}
