﻿using System.Collections.Generic;
using System.IO;

namespace EvaluationServer
{
    abstract class File
    {
        protected StreamReader sr;
        protected StreamWriter sw;
        protected string location;

        protected void init()
        {
            if (!System.IO.File.Exists(location))
                System.IO.File.Create(location).Close();
        }

        protected List<string> read()
        {
            List<string> val = new List<string>();
            try
            {
                sr = new StreamReader(location);
            }
            catch (System.IO.FileNotFoundException)
            {
                System.IO.File.Create(location).Close();
                sr = new StreamReader(location);
            }
            string line;
            while ((line = sr.ReadLine()) != null)
                val.Add(line);
            sr.Close();
            return val;
        }

        protected void write(List<string> val)
        {
            sw = new StreamWriter(location);
            int i = 0;
            while(i < val.Count)
            {
                sw.WriteLine(val[i]);
                i++;
            }
            sw.Close();
        }

    }
}
