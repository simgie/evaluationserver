﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace EvaluationServer
{
    class Program
    {
        /*
         *  QUELLE: https://www.youtube.com/watch?v=xgLRe7QV6QI
         */

        private static byte[] _buffer = new byte[1024];
        private static List<Socket> _clientSockets = new List<Socket>();
        private static Socket _serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private static Config config;
        private static string configfile = "fragen.txt";  // TODO: CHANGEME
        private static Answer answer;
        private static string answerfile = "antworten.txt"; // TODO: CHANGEME
        private static int clientcount = 0;

        static void Main(string[] args)
        {
            Console.Title = "Server";
            SetupServer();
            Console.Write("> ");
            string input = Console.ReadLine();
            while (input != "stop")
            {
                if(input.ToLower() == "reload")
                {
                    config = new Config(configfile);
                    Console.WriteLine("Konfiguration wurde erfolgreich neu geladen.");
                }

                Console.Write("> ");
                input = Console.ReadLine();
            }
        }

        private static void SetupServer()
        {
            Console.WriteLine("Setting up server...");
            if (!System.IO.File.Exists(configfile))
            {
                Console.WriteLine("Die Datei {0} wurde nicht gefunden.\n Eine neue Datei wurde erstellt. Diese gegebenenfalls anpassen.", configfile); 
                System.IO.File.Create(configfile).Close();
            }
            config = new Config(configfile);
            answer = new Answer(answerfile);
            Console.Write("Auf welchem Port soll der Server laufen? [100] : ");
            int port;
            try
            {
                var portstr = Console.ReadLine();
                if (portstr == "")
                    portstr = "100";
                port = Convert.ToInt32(portstr);
            }
            catch (Exception)
            {

                Console.WriteLine("Falsche Eingabe...");
                //Console.WriteLine("Beende... [Beliebige Taste Drücken]");
                SetupServer(); // Methoden-Neustart bei Falscheingabe
                return;
            }
            try
            {
                _serverSocket.Bind(new IPEndPoint(IPAddress.Any, port));
                _serverSocket.Listen(1);
                _serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);
                Console.WriteLine("Server wurde erfolreich erstellt. {0}:{1}", GetLocalIPAddress(), port);
                
            }
            catch (SocketException)
            {
                Console.WriteLine("Der angegebene Port konnte nicht verwendet werden. Eventuell belegt eine andere Anwendung diesen Port bereits.");
                Console.WriteLine("Beende... [Beliebige Taste Drücken]");
                return;
            }
        }

        /// <summary>
        /// Get's the local IP-Adress (Source: StackOverflow)
        /// </summary>
        /// <returns>Local IP</returns>
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        private static void AcceptCallback(IAsyncResult AR)
        {
            Socket socket = _serverSocket.EndAccept(AR);
            _clientSockets.Add(socket);
            IPEndPoint remoteIpEndPoint = socket.RemoteEndPoint as IPEndPoint;
            Console.WriteLine("Client connected from {0}. Unique Client-ID: {1}", remoteIpEndPoint.Address, clientcount);
            socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReciveCallback), socket);
            _serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);
        }

        private static void ReciveCallback(IAsyncResult AR)
        {
            Socket socket = (Socket)AR.AsyncState;
            try
            {
                int received = socket.EndReceive(AR);
                byte[] dataBuf = new byte[received];
                Array.Copy(_buffer, dataBuf, received);

                string text = Encoding.ASCII.GetString(dataBuf);
                Console.WriteLine("Text recieved: " + text);

                if (text == "get time")
                    SendText(DateTime.Now.ToLongTimeString(), socket);
                else if (text == "handshake")
                {
                    SendText(clientcount.ToString(), socket);
                    clientcount++;
                }
                else if (text.Contains("q"))
                    SendText(config.get(text), socket);
                else if (text.Contains("a"))
                {
                    answer.add(text); //TODO: Mehr Features. z.B. HTML-Tabelle Live 
                    SendText("OK", socket);
                }
                else
                    SendText("Invalid Request", socket);
                //switch(text)
                //{
                //    case "get time" : SendText(DateTime.Now.ToLongTimeString(), socket); break;
                //    case "q1" : SendText(config.get("q1"), socket); break;
                //    default : SendText("Invalid Request", socket); break;
                //}



                socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReciveCallback), socket);
                _serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);
            }
            catch (SocketException)
            {
                _clientSockets.Remove(socket);
                IPEndPoint remoteIpEndPoint = socket.RemoteEndPoint as IPEndPoint;
                Console.WriteLine("Client disconnected from {0}, {1} Client(s) left.", remoteIpEndPoint.Address, _clientSockets.Count);
                return;
            }
            
          
        }

        private static void SendText(string text, Socket socket)
        {
            if (text.Contains("ä")) //TODO: MEHR UMLAUTE
            {
                text = text.Replace("ä", "auml;");
            }
            byte[] data = Encoding.ASCII.GetBytes(text);
            socket.BeginSend(data, 0, data.Length, SocketFlags.None, new AsyncCallback(SendCallback), socket);
        }

        private static void SendCallback(IAsyncResult AR)
        {
            Socket socket = (Socket)AR.AsyncState;
            socket.EndSend(AR);
        }
    }
}
